import inflect
from datetime import date
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

class PopularityPoint(admin.SimpleListFilter):
    title = _('Points')

    parameter_name = 'popularity points'

    def lookups(self, request, model_admin):
        lookup_param = self.get_lookups()
        return lookup_param

    def get_lookups(self):
        lookup_interval = [i for i in range(1000) if i % 100 == 0]
        lookup_tuple = []
        for each_integer in lookup_interval:
            p = inflect.engine()
            each_word = p.number_to_words(each_integer).title()+'+'
            lookup_tuple.append((each_integer,each_word))
        return lookup_tuple

    def queryset(self, request, queryset):
        filter_value = self.value()
        if filter_value:
            return queryset.filter(popularity_point__gte = filter_value)

class MostUnliked(admin.SimpleListFilter):
    title = _('Unliked Points')

    parameter_name = 'unlike points'

    def lookups(self, request, model_admin):
        lookup_param = self.get_lookups()
        return lookup_param

    def get_lookups(self):
        lookup_interval = [i for i in range(100) if i % 100 == 0]
        lookup_tuple = []
        for each_integer in lookup_interval:
            p = inflect.engine()
            each_word = p.number_to_words(each_integer).title()+'+'
            lookup_tuple.append((each_integer,each_word))
        return lookup_tuple

    def queryset(self, request, queryset):
        filter_value = self.value()
        if filter_value:
            return queryset.filter(unlike_count__gte = filter_value)

