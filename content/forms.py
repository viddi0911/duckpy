from django.forms import ModelForm
from content.models import Image, Tag


class UploadImageForm(ModelForm):
    #form.fields['img_path'].widget = forms.HiddenInput()

    class Meta:
        model = Image
        fields = ['name', 'title', 'content_rating', 'img_path', 'tag']


class TagForm(ModelForm):

    class Meta:
        model = Tag
        fields = ['name']
