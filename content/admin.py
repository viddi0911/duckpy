from django.contrib import admin

from content.models import Tag,Image 
from content.custom_list_filter import PopularityPoint,MostUnliked

class TagAdmin(admin.ModelAdmin):
    model = Tag 
    list_display = ('name','slug')

class ImageAdmin(admin.ModelAdmin):
    model = Image 
    list_display = ('original_name', 'content_rating' ,'like_count','share_count','content_rating','popularity_point','unlike_count')
    list_filter = ('unabused',PopularityPoint,MostUnliked)
    readonly_fields = ('image_url',)

    def image_pic(self,instance):
        if instance.image_url:
            return u'<img src="%s" />' % instance.image_url
        else:
            return "sorry no image found"
        image_pic.allow_tags = True
        image_pic.short_description = "Image"

admin.site.register(Tag,TagAdmin)
admin.site.register(Image,ImageAdmin)
