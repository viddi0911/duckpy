from django.db import models
from django.core.cache import cache


class ImageManager(models.Manager):

    def get_all_stream(self):
        all_stream = cache.get('get-all-cached-category', None)
        if not all_stream:
            all_stream = self.all()
            cache.set('get-all-cached-category', all_stream, 7 * 24 * 60 * 60)
        return all_stream
