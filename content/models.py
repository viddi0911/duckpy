from django.db import models
#from django.db.models.signals import post_delete, post_save
from django.contrib.auth.models import User
from content.choices import RATING
from django.template.defaultfilters import slugify
from content.snippets import unique_slugify
#from managers import StreamManager


class Tag(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        slug = u"%s" % (slugify(self.name))
        super(Tag, self).save(**kwargs)


class Image(models.Model):

    name = models.CharField(max_length=150, null=True, blank=True)
    original_name = models.CharField(max_length=150, null=True, blank=True)
    title = models.CharField(max_length=150, null=True, blank=True)
    slug = models.SlugField(unique=True, blank=True)
    content_rating = models.CharField(
        max_length=2, choices=RATING, default='1')
    img_path = models.ImageField(upload_to='media/funnypics/')
    image_url = models.CharField(max_length=150, null=True, blank=True)
    tagged_by = models.ForeignKey(User, null=True, blank=True)
    unabused = models.BooleanField(default=1)
    tag = models.ManyToManyField(Tag)
    view_count = models.IntegerField(default=0)
    comment_count = models.IntegerField(default=0)
    share_count = models.IntegerField(default=0)
    unlike_count = models.IntegerField(default=0)
    like_count = models.IntegerField(default=0)
    popularity_point = models.IntegerField(default=0)
    source = models.CharField(max_length=150, null=True, blank=True)
    active = models.BooleanField(default=1)

    def __unicode__(self):
        return unicode(self.title) or u''

    def save(self, **kwargs):
        slug = u"%s" % (slugify(self.title))
        unique_slugify(self, slug)
        super(Image, self).save(**kwargs)
