from django.conf.urls import patterns, include, url

from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', 'picypy.views.home', name='home'),
                       url(r'^page/(?P<page>\d+)/$',
                           'picypy.views.home', name='home'),
                       url(r'^random/$', 'picypy.views.random_image',
                           name='random_image'),
                       url(r'^random/(?P<img>.*)/$',
                           'picypy.views.random_image', name='random_image'),
                       url(r'^image/(?P<img>.*)',
                           'picypy.views.image_detail', name='image_detail'),
                       url(r'^get-images-by-ajax$', 'picypy.views.get_images_by_ajax',
                           name='get_images_by_ajax'),
                       url(r'^rate-image/$', 'picypy.views.rate_image',
                           name='rate_image'),
                       url(r'^privacy/$', 'picypy.views.privacy',
                           name='privacy'),
                       url(r'^contact/$', 'picypy.views.contact',
                           name='contact'),
                       url(r'^aboutus/$', 'picypy.views.aboutus',
                           name='aboutus'),
                       url(r'^popular/$', 'picypy.views.popular',
                           name='popular'),
                       url(r'^trending/$', 'picypy.views.trending',
                           name='trending'),
                       url(r'^hot/$', 'picypy.views.hot', name='hot'),
                       #(r'^accounts/', include('registration.backends.default.urls')),
                       #url(r'^content/', include('content.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': settings.STATIC_ROOT}),
                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': settings.MEDIA_ROOT}),
                       )

