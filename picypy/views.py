import urllib
import json
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response, RequestContext, HttpResponse, HttpResponseRedirect
from content.forms import UploadImageForm, TagForm
from content.models import Image
from core.redis_images import get_ibucket_images, get_ibucket_images_by_ajax, update_like_unlike_count, update_view_count, get_related_images, get_user_interest_images
from core.utils import get_images_by_id
import os
import redis
import random
import uuid

SIDEBAR_IMAGES_NO = 5


def home(request, page_no=0):
    random_images_id = get_ibucket_images(page_no)
    random_images = get_images_by_id(random_images_id)
    top_images = get_top_images()
    user_id = request.COOKIES.get('duckid')
    _user_interest_image_ids = get_user_interest_images(user_id, 5)
    suggested_image = get_images_by_id(_user_interest_image_ids)
    if len(suggested_image) == 0:
        suggested_image = Image.objects.order_by('-popularity_point')[0:5]
    response = render_to_response(
        'index.html', locals(), context_instance=RequestContext(request))
    if user_id is None:
        response.set_cookie(
            key='duckid', value=uuid.uuid4().int, expires=None, path='/')
    else:
        pass
    return response


def random_image(request, img=None):
    user_id = request.COOKIES.get('duckid')
    if request.COOKIES.get('duckid') is None:
        user_id = uuid.uuid4().int
    else:
        user_id = request.COOKIES.get('duckid')
    if img is not None and img.isdigit():
        _next_image = get_ibucket_random_image(no_of_image=1)
        update_view_count(_next_image)
        next_image_id = _next_image[0]
        image = Image.objects.get(id=int(img))
        next_image = Image.objects.get(id=next_image_id)
    elif img is not None and img.isalpha():
        _next_image = get_ibucket_random_image(no_of_image=1)
        update_view_count(_next_image)
        next_image_id = _next_image[0]
        next_image = Image.objects.get(id=next_image_id)
        image = Image.objects.get(slug=img)
    else:
        _random_images = get_ibucket_random_image(no_of_image=2)
        next_image_id = _random_images[0]
        update_view_count(_random_images[1:])
        current_image_id = _random_images[1]
        image = Image.objects.get(id=current_image_id)
        next_image = Image.objects.get(id=next_image_id)
    suggested_image = get_related_image_analyzed(image.id, user_id)
    if len(suggested_image) == 0:
        suggested_image = Image.objects.order_by('-popularity_point')[0:5]

    RANDOM_IMAGE = True

    response = render_to_response('image_detail.html', locals(),
                                  context_instance=RequestContext(request))

    if request.COOKIES.get('duckid') is None:
        response.set_cookie(
            key='duckid', value=user_id, expires=None, path='/')
    return response


def image_detail(request, img=None):

    user_id = request.COOKIES.get('duckid')
    if request.COOKIES.get('duckid') is None:
        user_id = uuid.uuid4().int
        # response.set_cookie(key='duckid',value=user_id,expires=None,path='/')
    else:
        pass
    if img.isdigit():
        image = Image.objects.get(id=int(img))
    elif img.isalpha():
        image = Image.objects.get(slug=img)

    suggested_image = get_related_image_analyzed(image.id, user_id)
    if len(suggested_image) == 0:
        suggested_image = Image.objects.order_by('-popularity_point')[0:5]

    response = render_to_response('image_detail.html', locals(),
                                  context_instance=RequestContext(request))
    if request.COOKIES.get('duckid') is None:
        response.set_cookie(
            key='duckid', value=user_id, expires=None, path='/')
    return response


def popular(request):
    # paying for wrong project structure
    random_images = Image.objects.order_by('-popularity_point')[0:5]
    top_images = get_top_images()
    return render_to_response('index.html', locals(),
                              context_instance=RequestContext(request))


def trending(request):
    random_images = Image.objects.filter(
        popularity_point__gte=1).order_by('popularity_point')[0:5]
    top_images = get_top_images()
    return render_to_response('index.html', locals(),
                              context_instance=RequestContext(request))


def hot(request):
    random_images = Image.objects.order_by('-lovely')[0:5]
    top_images = get_top_images()
    return render_to_response('index.html', locals(),
                              context_instance=RequestContext(request))


def get_images_by_ajax(request):
    if request.is_ajax():
        current_url_link = request.META['HTTP_REFERER']
        images_shown = json.loads(request.GET.get('images_shown'))
        if 'popular' in current_url_link:
            random_images = Image.objects.exclude(
                id__in=images_shown).order_by('-popularity_point')[0:5]
        elif 'trending' in current_url_link:
            random_images = Image.objects.exclude(id__in=images_shown).filter(
                popularity_point__gte=1).order_by('popularity_point')[0:5]
        elif 'hot' in current_url_link:
            random_images = Image.objects.exclude(
                id__in=images_shown).order_by('-lovely')[0:5]
        else:
            last_img_shown = int(images_shown[-1].split('_')[0])
            bucket_images_id = get_ibucket_images_by_ajax(last_img_shown)
            random_images = get_images_by_id(bucket_images_id)
    return render_to_response('display_images.html', locals(),
                              context_instance=RequestContext(request))


def get_top_images():
    top_images = Image.objects.order_by('-like_count')[0:7]
    return top_images


def get_ibucket_random_image(no_of_image=1):
    r_server = redis.Redis("localhost")
    bucket_images = r_server.lrange('iBucket', 0, -1)
    random.shuffle(bucket_images)
    return bucket_images[-no_of_image:]


def get_related_image_analyzed(image_id, user_id):
    related_images_id = get_related_images(image_id, user_id)
    no_of_missing_images = SIDEBAR_IMAGES_NO
    if len(related_images_id) < SIDEBAR_IMAGES_NO:
        random_images_id = get_ibucket_images(0)
        no_of_missing_images = SIDEBAR_IMAGES_NO - len(random_images_id)
        random_images_id[0:no_of_missing_images]
        for image in random_images_id:
            related_images_id.append(image)
    suggested_image = get_images_by_id(related_images_id)
    return suggested_image[0:no_of_missing_images]


def rate_image(request):
    if request.is_ajax():
        image_id = request.GET.get('id')
        rating = int(image_id.split('_')[1])
        image_id = int(image_id.split('_')[0])
        user_id = request.COOKIES.get('duckid', None)
        #rating_image = Image.objects.get(id=image_id)
        new_rated_css = ''
        if rating == 1:
            new_rated_css = 'typcn typcn-thumbs-up rated'
            like_count = update_like_unlike_count(image_id, user_id, like=True)
        elif rating == 2:
            like_count = update_like_unlike_count(
                image_id, user_id, unlike=True)
            new_rated_css = 'typcn typcn-thumbs-up rated'
        elif rating == 3:
            like_count = update_like_unlike_count(
                image_id, user_id, comment=True)
            new_rated_css = 'typcn typcn-message rated rating-share'
        elif rating in (4, 5, 6):
            like_count = update_like_unlike_count(
                image_id, user_id, share=True)
            if rating == 4:
                new_rated_css = 'typcn typcn-social-facebook rated rating-share'
            elif rating == 5:
                new_rated_css = 'typcn typcn-social-google-plus rated rating-share'
            elif rating == 6:
                new_rated_css = 'typcn typcn-social-twitter rated rating-share'
        elif rating == 7:
            like_count = update_like_unlike_count(
                image_id, user_id, clicked=True)
            new_rated_css = 'img-div rated'

        # rating_image.save()
        # update like count from redis
        #like_count = int(rating_image.like_count)
        if like_count == 0:
            like_count = '0 Like'
        elif like_count == 1:
            like_count = '1 Like'
        else:
            like_count = '{0} Likes'.format(like_count)
    ajax_response = {'new_class_name': new_rated_css, 'like_count': like_count}
    ajax_response_json = json.dumps(ajax_response)
    return HttpResponse(ajax_response_json, content_type='application/json')


def privacy(request):
    return render_to_response('privacy.html', locals(), context_instance=RequestContext(request))


def contact(request):
    return render_to_response('contact.html', locals(), context_instance=RequestContext(request))


def aboutus(request):
    return render_to_response('about.html', locals(), context_instance=RequestContext(request))
