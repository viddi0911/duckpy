"""
WSGI config for picypy project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys
from django.conf import settings
project_path = '/home/vidyasagar/workspace/duckypy'
if project_path not in sys.path:
    sys.path.append(project_path)
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "picypy.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
