from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from django.conf import settings
from content.models import Image, Tag
import os
from content.models import Image
#from django.core.cache import cache
#from redis_cache import get_redis_connection
import redis


class Command(BaseCommand):

    def handle(self, *args, **options):
        all_images = Image.objects.order_by('?').values_list('id')[0:3]
        _new_bucket_image = [image[0] for image in all_images]
        r_server = redis.Redis(host="localhost", port=6379, db=0)
        bucket_images = r_server.lrange('iBucket', 0, -1)
        _bucket_images = [int(_img) for _img in bucket_images]
        ######### add a condition so that if images on bucket is not show repeat showing same images and stop population 
        for img_id in _new_bucket_image:
            if img_id not in _bucket_images:
                r_server.lpush('iBucket', img_id)
                related_images = []
                image_detail = {img_id: {'view': 0, 'like': 0, 'unlike': 0, 'comment': 0,
                                         'share': 0, 'popularity_point': 0, 'clicked': 0, 'related_images': related_images}}
                r_server.hmset('images_detail', image_detail)
        print 'iBucket Filled '
