from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from django.conf import settings

from content.models import Image, Tag
import os

AWS_ACCESS_KEY = settings.AWS_ACCESS_KEY
AWS_SECRET_ACCESS_KEY = settings.AWS_SECRET_ACCESS_KEY
S3_URL = settings.S3_URL


class Command(BaseCommand):

    def handle(self, *args, **options):
        """  
        management command to put new file in s3bucket and a command to insert into database 
        1.get all images from directory funnyimage and 
        2.change name of images as per datetime now and 
        3.make an entry in database with the same name and original name
        4. for each images in funnyimages with changed name put images in s3 bucket and get that s3 url and save url in database  
        """
        from boto.s3.connection import S3Connection
        conn = S3Connection(AWS_ACCESS_KEY, AWS_SECRET_ACCESS_KEY)
        bucket_objects = conn.get_bucket('picypy')
        all_images = bucket_objects.get_all_keys()
        i = 1
        for each_image in all_images:
            image_name = each_image.key.split('/')[-1]
            image_url = S3_URL + image_name
            # if 'images' in image_name:
            image, created = Image.objects.get_or_create(
                name=each_image.key, s3_url=image_url)
            if created:
                print "\n\n\t%s Saved Image %s  " % (i, each_image.key)
            else:
                print "\n\n\t %s Could Not saved Image %s  " % (i, each_image.key)
            i += 1
