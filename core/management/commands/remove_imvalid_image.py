from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from django.conf import settings
from content.models import Image, Tag
import os
import sys
from content.models import Image
#from django.core.cache import cache
#from redis_cache import get_redis_connection
import redis


class Command(BaseCommand):

    def handle(self, *args, **options):
        sys.path.append(os.path.dirname(os.path.dirname(__file__)))
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "picypy.settings")
        _images_path = Image.objects.all().values_list('img_path')
        images_path = [image[0] for image in _images_path]
        for each in images_path:
            try:
                if os.path.getsize(each) == 0:
                    Image.objects.get(img_path=each).delete()
                    print "deleted image with path : %s as it is zero size image" % (each)
            except:
                if not os.path.isfile(each):
                    Image.objects.get(img_path=each).delete()
                    print "deleted image with path : %s" % (each)
