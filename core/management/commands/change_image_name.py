from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from django.conf import settings

from content.models import Image, Tag
import os


class Command(BaseCommand):

    def handle(self, *args, **options):
        """ List off images in a text file named images_name.txt , further read each image and change the name of image and save image name and current location of images """
        image_path = settings.PROJECT_PATH + \
            '/' + settings.MEDIA_URL + 'images/'
        img_file_name = settings.PROJECT_PATH + '/' + \
            settings.MEDIA_URL + 'images_name.txt'
        img_file_name_cmd = 'ls ' + image_path + ' > ' + img_file_name
        os.system(img_file_name_cmd)
        images = open(img_file_name, 'r').readlines()
        upload_path = image_path + 'funnypics/'
        i = 1
        # image_path doesnot store full path in database
        image_path_db = settings.MEDIA_URL + 'images/funnypics/'
        for image in images:
            old_image_name = image.strip('\n')
            new_file_name = str(
                i) + '_' + datetime.now().strftime("%y%m%d_%H%M%S") + '.' + old_image_name.split('.')[-1]

            new_image_path = image_path + '/funnypics/' + new_file_name
            new_image_path_db = image_path_db + new_file_name

            old_image_path = image_path
            move_upload_cmd = 'mv ' + old_image_path + \
                old_image_name + ' ' + new_image_path
            os.system(move_upload_cmd)
            image, created = Image.objects.get_or_create(
                name=old_image_name, img_path=new_image_path_db)
            if created:
                print "\n\n\t%s Saved Image %s  " % (i, old_image_name)
            else:
                print "\n\n\t %s Could Not saved Image %s  " % (i, old_image_name)
            # if i==5:
            #    break
            i = i + 1
