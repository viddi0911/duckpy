import os 
import time
import uuid
import cloudinary
from cloudinary import uploader
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from django.conf import settings 
from content.models import Image 
from cloudinary_config import CLOUDINARY_CONFIG

"""
Algo :
    Get all images from dirname image_bucket in media , 
    change image name ,
    upload to bucket picypy in cloudinary , 
    if upload fails , move that image in a directory failed_images 
    elif success :
        make entry in database with image url  
        remove images from current directory 
"""

IMAGE_BUCKET_PATH = settings.IMAGE_BUCKET_PATH
FAILED_IMAGES_DIR = settings.FAILED_IMAGES_DIR
DELETE_ALL = True

class Command(BaseCommand):

    def handle(self, *args, **options):
        all_cloud_images = cloudinary.api.resources()
        _bucket_path = IMAGE_BUCKET_PATH
        _cloud_bucket_images = get_all_images(_bucket_path) 
        for _each_images in _cloud_bucket_images:
            img_obj = Image(original_name=_each_images)
            new_images_name = str(uuid.uuid4())+ '.' +_each_images.split('.')[-1]
            change_image_name(IMAGE_BUCKET_PATH+_each_images,IMAGE_BUCKET_PATH+new_images_name)
            img_obj.name = new_images_name
            new_image_path = _bucket_path + new_images_name
            cloud_response = uploader.upload(new_image_path)
            if cloud_response.get('url'):
                img_obj.image_url = cloud_response.get('url')
                print "uploaded images %s" %(_each_images)
                img_obj.save()
                os.remove(new_image_path)
            else:
                print "failed uploading images %s" %(_each_images)
                change_image_name(IMAGE_BUCKET_PATH+new_image_path,FAILED_IMAGES_DIR+_each_images)

def get_all_images(_bucket_path):
    _images_in_bucket = []
    for name in os.listdir(_bucket_path):
        if os.path.isfile(os.path.join(_bucket_path, name)):
            _images_in_bucket.append(name)
    return _images_in_bucket

def change_image_name(image_1,image_2):
    return os.rename(image_1,image_2)
    
def delete_all():
    if DELETE_ALL:
        for each_cloud_images in all_cloud_images['resources']:
            cloudinary.api.delete_resources(each_cloud_images['public_id'])
