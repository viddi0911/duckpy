from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from django.conf import settings
from content.models import Image, Tag
import os
from content.models import Image
#from django.core.cache import cache
#from redis_cache import get_redis_connection
import redis


class Command(BaseCommand):

    def handle(self, *args, **options):
        # get all images from ibucket and update in database
        r_server = redis.Redis("localhost")
        bucket_images = r_server.lrange('iBucket', 0, -1)
        _bucket_images = [int(_img) for _img in bucket_images]
        for img_id in _bucket_images:
            print "Processiong image %s" % (img_id)
            image_detail = eval(r_server.hmget('images_detail', img_id)[0])
            image_db = Image.objects.get(id=img_id)
            image_db.view_count = image_detail.get('view', 0)
            image_db.like_count = image_detail.get('like', 0)
            image_db.unlike_count = image_detail.get('unlike', 0)
            image_db.share_count = image_detail.get('share', 0)
            image_db.comment_count = image_detail.get('comment', 0)
            image_db.popularity_point = get_poularity_pont(image_detail)
            image_db.save()


def get_poularity_pont(image_detail):
    popularity_point = image_detail[
        'like'] * 2 + image_detail['comment'] + image_detail['share'] * 5 - image_detail['unlike']
    return popularity_point
