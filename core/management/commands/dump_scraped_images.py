from datetime import datetime, timedelta

from django.core.management.base import BaseCommand

from content.models import Image, Tag


class Command(BaseCommand):

    def handle(self, *args, **options):
        print "\n\n\tInitializing Saving Images " % (datetime.now())
        for test in all_active_test:
            testresult = TestResult.objects.filter(
                test=test, status='Completed')
            total_student = testresult.count()
            print "------\n%d students to be updated for test %d-%s..." % (total_student, test.id, test.name)
            _query = "SELECT t1.id, count(t2.id) FROM reports_testresult t1 left outer join reports_testresult t2 on t1.test_id = t2.test_id WHERE t1.marks_obtained > t2.marks_obtained and t1.test_id = %d and t1.status = 'Completed' and t2.status = 'Completed' group by t1.id;"
            less_count_data = dict(get_data(_query % (test.id)))

            for result in testresult:
                less_count = less_count_data.get(result.id, 0)
                result.percentile = "{0:.2f}".format(
                    less_count * 100.0 / total_student)
                result.rank = round(total_student - less_count)
                result.save()
            print "update successfully done."
