import redis
import random
from content.models import Image
SIDEBAR_IMAGES_NO = 5


def get_ibucket_images(page_no):
    """ Get images from iBucket on the basic of page number user is visiting """

    r_server = redis.Redis("localhost")
    start_index = page_no * 5
    end_index = start_index + 4
    bucket_images_id = r_server.lrange('iBucket', start_index, end_index)
    update_view_count(bucket_images_id)
    return bucket_images_id


def get_ibucket_images_by_ajax(last_image_shown):
    """ Get images from iBucket for each ajax """

    r_server = redis.Redis("localhost")
    total_images = r_server.llen('iBucket')
    next_image_index = 0
    for img_index in range(4, total_images, 5):
        if int(last_image_shown) == int(r_server.lindex('iBucket', img_index)):
            next_image_index = int(img_index) + 1
            break
    bucket_images_id = r_server.lrange(
        'iBucket', next_image_index, next_image_index + 4)
    like_count = update_view_count(bucket_images_id)
    return bucket_images_id


def update_view_count(bucket_images_id):
    """ Update view count when each image is shown to user """
    r_server = redis.Redis("localhost")
    for image in bucket_images_id:
        like_count = update_image_detail(image, r_server, view=1)
    return True


def update_image_detail(image, r_server, **kwargs):
    """ Update image detail dor wach image i.e it update like , unlike , etc"""
    image_detail = eval(r_server.hmget('images_detail', image)[0])
    each_img_detail = {image: image_detail}
    if kwargs.get('view'):
        view_count = image_detail.get('view')
        view_count += 1
        image_detail['view'] = view_count
    elif kwargs.get('like'):
        like_count = image_detail.get('like')
        like_count += 1
        image_detail['like'] = like_count
        current_image = Image.objects.get(id=image)
        current_image.like_count = like_count
        current_image.save()
    elif kwargs.get('unlike'):
        unlike_count = image_detail.get('unlike')
        like_count = image_detail.get('like')
        unlike_count += 1
        like_count -= 1
        image_detail['unlike'] = unlike_count
        image_detail['like'] = like_count
        current_image = Image.objects.get(id=image)
        current_image.like_count = like_count
        current_image.save()
    elif kwargs.get('comment'):
        comment_count = image_detail.get('comment')
        comment_count += 1
        image_detail['comment'] = comment_count
        current_image = Image.objects.get(id=image)
        current_image.like_count = like_count
        current_image.save()
    elif kwargs.get('clicked'):
        clicked_count = image_detail.get('clicked')
        clicked_count += 1
        image_detail['clicked'] = clicked_count
    elif kwargs.get('share'):
        share_count = image_detail.get('share')
        share_count += 1
        image_detail['share'] = share_count
    elif kwargs.get('related_images'):
        related_images = image_detail.getlist('related_images')
        current_related_image = kwargs.getlist('related_images')
        for _image in current_related_images:
            if _image not in related_images:
                related_images.append(_image)
        image_detail['related_images'] = related_images
    each_img_detail = {image: image_detail}
    like_count = image_detail.get('like')
    r_server.hmset('images_detail', each_img_detail)
    return like_count


def update_like_unlike_count(image, user_id=None, comment=False, like=False, unlike=False, share=False, clicked=False):
    """ Based on user interest over a image increase like and unlike count """
    r_server = redis.Redis("localhost")
    img_id = image
    if like:
        like_count = update_image_detail(image, r_server, like=1)
        if user_id:
            update_user_image_interest(user_id, img_id, like=True)
    elif unlike:
        like_count = update_image_detail(image, r_server, unlike=1)
        update_user_image_interest(user_id, img_id, unlike=True)
    elif share:
        like_count = update_image_detail(image, r_server, share=1)
        update_user_image_interest(user_id, img_id, share=True)
    elif clicked:
        like_count = update_image_detail(image, r_server, clicked=1)
        update_user_image_interest(user_id, img_id, clicked=True)
    return like_count


def update_user_image_interest(user_id, img_id, like=False, unlike=False, share=False, clicked=False):
    """Put liked images in a list again that particular user and maintain a list of unliked images """

    r_server = redis.Redis("localhost")
    user_img_redis_key = 'user_img_{0}'.format(user_id)
    if not unlike and str(img_id) not in r_server.lrange(user_img_redis_key, 0, -1):
        r_server.lpush(user_img_redis_key, img_id)
    elif unlike:
        r_server.lpush('unlike_image', img_id)
    user_images = get_image_liked_by_user(user_id)
    related_image = update_related_image(user_images, r_server)
    return True


def get_image_liked_by_user(user_id):
    r_server = redis.Redis("localhost")
    user_img_redis_key = 'user_img_{0}'.format(user_id)
    user_image_id = r_server.lrange(user_img_redis_key, 0, -1)
    return user_image_id


def update_related_image(user_images, r_server):
    for each_image in user_images:
        _related_image = eval(
            r_server.hmget('images_detail', each_image)[0])['related_images']
        for img in user_images:
            if img not in _related_image and img is not each_image:
                _related_image.append(img)
        _image_detail = eval(r_server.hmget('images_detail', each_image)[0])
        _image_detail['related_images'] = _related_image
        r_server.hmset('images_detail', {each_image: _image_detail})
    return True


def get_related_images(img_id, user_id):
    r_server = redis.Redis("localhost")
    try:
        _related_image = eval(
            r_server.hmget('images_detail', img_id)[0])['related_images']
    except:
        _related_image = []
    if len(_related_image) < SIDEBAR_IMAGES_NO:
        no_of_related_image = len(_related_image)
        no_of_missing_image = SIDEBAR_IMAGES_NO - no_of_related_image
        _user_interest_images = get_user_interest_images(
            user_id, no_of_missing_image)
        for _image in _user_interest_images:
            _related_image.append(_image)
        random.shuffle(_related_image[0:SIDEBAR_IMAGES_NO])
    return _related_image


def get_user_interest_images(user_id, no_of_missing_image):
    r_server = redis.Redis("localhost")
    user_img_redis_key = 'user_img_{0}'.format(user_id)
    user_images = r_server.lrange(user_img_redis_key, 0, -1)
    random.shuffle(user_images)
    user_related_image = []
    for each_image in user_images:
        try:
            _related_image = eval(
                r_server.hmget('images_detail', each_image)[0])['related_images']
        except:
            _related_image = []
        for image in _related_image:
            if image not in user_images:
                user_related_image.append(image)
    return user_related_image[0:no_of_missing_image]
