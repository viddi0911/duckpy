from django.shortcuts import render
from content.models import Image

# Create your views here.


def check_cookies_set(main_dict, search_key):
    match_found = []
    for each_key in main_dict:
        if search_key in each_key:
            match_found.append({search_key: main_dict[search_key]})
    return match_found


def get_images_by_id(random_images_id):
    ordering = 'FIELD(`id`, %s)' % ','.join(str(id) for id in random_images_id)
    ordered_images = Image.objects.filter(id__in=random_images_id).extra(
        select={'ordering': ordering}, order_by=('ordering',))
    return ordered_images
