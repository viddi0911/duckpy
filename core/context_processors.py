#!/usr/bin/env python
from django.conf import settings

def static( context ):
    return {'STATIC_URL': settings.STATIC_URL}
